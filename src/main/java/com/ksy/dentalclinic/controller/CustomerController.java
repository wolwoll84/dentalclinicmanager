package com.ksy.dentalclinic.controller;

import com.ksy.dentalclinic.model.CustomerInfoUpdateRequest;
import com.ksy.dentalclinic.model.CustomerItem;
import com.ksy.dentalclinic.model.CustomerRequest;
import com.ksy.dentalclinic.model.ReservationUpdateRequest;
import com.ksy.dentalclinic.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "정보가 입력되었습니다.";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        return customerService.getCustomers();
    }

    @PutMapping("/info/id/{id}")
    public String putInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putInfo(id, request);

        return "개인정보 수정되었습니다.";
    }

        @PutMapping("/reservation/id/{id}")
    public String putReservation(@PathVariable long id, @RequestBody @Valid ReservationUpdateRequest request) {
        customerService.putReservation(id, request);

        return "예약 시간이 등록되었습니다.";
    }

    @PutMapping("/last/id/{id}")
    public String putLast(@PathVariable long id) {
        customerService.putDateLast(id);

        return "환영합니다.";
    }
}
