package com.ksy.dentalclinic.repository;

import com.ksy.dentalclinic.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
