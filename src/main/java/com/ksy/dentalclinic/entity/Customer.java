package com.ksy.dentalclinic.entity;

import com.ksy.dentalclinic.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String chartNumber;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 13)
    private String residentRegistrationNumber;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(nullable = false, length = 20)
    private String customerPhone;

    @Column(nullable = false, length = 35)
    private String customerAddress;

    private String information;
    private LocalDateTime reservationTime;

    @Column(nullable = false)
    private LocalDate visitFirst;

    private LocalDate visitLast;
}
