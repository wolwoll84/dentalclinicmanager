package com.ksy.dentalclinic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Revisit {
    READY("대기"),
    VISIT_REQUEST("방문요망"),
    EMERGENCY("안내문자 발송"),
    QUIESCENCE("휴면");
    private final String status;
}
