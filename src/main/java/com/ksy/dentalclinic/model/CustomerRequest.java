package com.ksy.dentalclinic.model;

import com.ksy.dentalclinic.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 12, max = 13)
    private String  residentRegistrationNumber;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @NotNull
    @Length(min = 7, max = 20)
    private String customerPhone;

    @NotNull
    @Length(min = 5, max = 35)
    private String customerAddress;

    private String information;
}
