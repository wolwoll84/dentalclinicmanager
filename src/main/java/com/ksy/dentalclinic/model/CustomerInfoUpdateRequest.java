package com.ksy.dentalclinic.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class CustomerInfoUpdateRequest {
    @Length(min = 2, max = 20)
    private String customerName;

    @Length(min = 7, max = 20)
    private String customerPhone;

    @Length(min = 5, max = 35)
    private String customerAddress;

    private String information;
}
