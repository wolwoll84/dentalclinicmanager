package com.ksy.dentalclinic.model;

import com.ksy.dentalclinic.enums.Revisit;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private String personalInfo;
    private String customerPhone;
    private String customerAddress;
    private String information;
    private Boolean didReservation;
    private LocalDateTime reservationTime;
    private LocalDate visitFirst;
    private LocalDate visitLast;
    private String Revisit;
    @Enumerated(EnumType.STRING)
    private String RevisitStatus;
}
