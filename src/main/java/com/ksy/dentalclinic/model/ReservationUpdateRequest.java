package com.ksy.dentalclinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class ReservationUpdateRequest {
    @NotNull
    private LocalDateTime reservationTime;
}
