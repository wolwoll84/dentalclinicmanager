package com.ksy.dentalclinic.service;

import com.ksy.dentalclinic.entity.Customer;
import com.ksy.dentalclinic.enums.Revisit;
import com.ksy.dentalclinic.model.CustomerInfoUpdateRequest;
import com.ksy.dentalclinic.model.CustomerItem;
import com.ksy.dentalclinic.model.CustomerRequest;
import com.ksy.dentalclinic.model.ReservationUpdateRequest;
import com.ksy.dentalclinic.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public static String numberGen(int len, int dupCd) {
        Random random = new Random();
        String numStr = "";

        for (int i = 0; i<len ; i++) {
            String ran = Integer.toString(random.nextInt(10));

            if (dupCd == 1) {
                numStr += ran;
            } else if (dupCd == 2) {
                if (!numStr.contains(ran)) {
                    numStr += ran;
                } else {
                    i -= 1;
                }
            }
        }

        return numStr;
    }

    public void setCustomer(CustomerRequest request) {
        String chartNumber = numberGen(6,2);

        Customer addData = new Customer();
        addData.setChartNumber(chartNumber);
        addData.setCustomerName(request.getCustomerName());
        addData.setResidentRegistrationNumber(request.getResidentRegistrationNumber());
        addData.setGender(request.getGender());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setCustomerAddress(request.getCustomerAddress());
        addData.setInformation(request.getInformation());
        addData.setVisitFirst(LocalDate.now());

        customerRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originData = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setPersonalInfo(item.getCustomerName() + " / " + item.getGender().getName() + " / " + item.getResidentRegistrationNumber());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setCustomerAddress(item.getCustomerAddress());
            addItem.setInformation(item.getInformation());
            addItem.setDidReservation(item.getReservationTime() == null ? false : true);
            addItem.setReservationTime(item.getReservationTime());
            addItem.setVisitFirst(item.getVisitFirst());
            addItem.setVisitLast(item.getVisitLast());

            if (item.getVisitLast() != null) {
                LocalDateTime dateLast = LocalDateTime.now();

                LocalDateTime dateStart = LocalDateTime.of(
                        item.getVisitLast().getYear(),
                        item.getVisitLast().getMonthValue(),
                        item.getVisitLast().getDayOfMonth(),
                        0,
                        0,
                        0
                );

                long betweenRevisit = ChronoUnit.DAYS.between(dateStart, dateLast);

                Revisit reVisit = Revisit.QUIESCENCE;
                if (betweenRevisit <= 30) {
                    reVisit = Revisit.READY;
                } else if (betweenRevisit <= 90) {
                    reVisit = Revisit.VISIT_REQUEST;
                } else if (betweenRevisit <= 365) {
                    reVisit = Revisit.EMERGENCY;
                }

                addItem.setRevisit("마지막 방문일로부터 " + betweenRevisit + "일 경과했습니다.");
                addItem.setRevisitStatus(reVisit.getStatus());
            }

            result.add(addItem);
        }

        return result;
    }

    public void putInfo(long id, CustomerInfoUpdateRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setCustomerName(request.getCustomerName());
        originData.setCustomerPhone(request.getCustomerPhone());
        originData.setCustomerAddress(request.getCustomerAddress());
        originData.setInformation(request.getInformation());

        customerRepository.save(originData);
    }

    public void putReservation(long id, ReservationUpdateRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setReservationTime(request.getReservationTime());

        customerRepository.save(originData);
    }

    public void putDateLast(long id) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setVisitLast(LocalDate.now());

        customerRepository.save(originData);
    }
}
